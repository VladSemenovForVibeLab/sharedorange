import { createRouter, createWebHistory } from 'vue-router'
import PeopleForm from '../components/PeopleForm.vue'
import ItemsForm from '../components/ItemsForm.vue'
import ResultsBill from '../components/ResultsBill.vue'

import WelcomeForm from '../components/WelcomeForm.vue'
// Определение маршрутов для приложения
const routes = [
  {
    path: '/',
    name: 'WelcomeForm', // Имя маршрута
    component: WelcomeForm, // Компонент, который будет отображаться при переходе по этому маршруту
  },
  {
    path: '/people',
    name: 'PeopleForm',
    component: PeopleForm
  },
  {
    path: '/items',
    name: 'ItemsForm',
    component: ItemsForm
  },
  {
    path: '/results',
    name: 'Results',
    component: ResultsBill
  }
]
// Создание экземпляра маршрутизатора
const router = createRouter({
  history: createWebHistory(), // Создание экземпляра маршрутизатора с использованием HTML5 History API
  routes // Подключение определенных выше маршрутов
})
export default router // Экспорт созданного экземпляра маршрутизатора