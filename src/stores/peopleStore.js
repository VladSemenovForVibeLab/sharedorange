import { defineStore } from 'pinia'
import {useItemsStore} from "@/stores/itemsStore";

export const usePeopleStore = defineStore('people', {
    state: () => ({
        people: [],    // Массив для хранения информации о людях
        allNames: [],  // Массив для хранения имен всех людей
    }),
    actions: {
        // Добавление новой записи о человеке в хранилище
        addPerson(name) {
            this.people.push({id: Date.now(), name, eatenItems: [],selectedItems:[]}) // Добавление объекта с данными человека
            this.allNames.push(name) // Добавление имени в массив всех имен
            savePeopleToLocalStorage()
        },
        // Удаление записи о человеке по его идентификатору
        removePerson(id) {
            this.people = this.people.filter((person) => person.id !== id) // Фильтрация массива, удаляя человека по его id
            // Сохранение обновленного списка людей в локальное хранилище
            savePeopleToLocalStorage()
        },
        // Проверка, является ли имя уникальным среди уже добавленных людей
        isUniquePerson(name) {
            return !this.people.some((person) => person.name === name)  // Проверка с помощью some(), возвращение true, если имя уникально
        },
        // Получение данных о человеке по его идентификатору
        getPersonById: (state) => (personId) => {
            return state.people.find((person) => person.id === personId) // Поиск человека по id
        },
        // Получение имени человека по его идентификатору
        getPersonName: (state) => (personId) => {
            const person = state.people.find((person) => person.id === personId) // Поиск человека по id
            return person ? person.name : '' // Возвращение имени, если человек найден, иначе возвращение пустой строки
        },
        // Получение массива всех имен людей
        getAllNames() {
            return this.allNames // Возвращение массива всех имен
        },
        // Рассчет долга для данного человека
        calculateDebt(personId) {
            const person = this.getPersonById(personId);
            if (person && person.eatenItems) {
                const itemsStore = useItemsStore();
                let debt = 0;
                for (const itemId of person.eatenItems) {
                    const item = itemsStore.items.find((item) => item.id === itemId);
                    if (item) {
                        debt += item.price / item.people.length;
                    }
                }
                return debt.toFixed(2);
            }
            return '0.00';
        },
    },
})
const savePeopleToLocalStorage = () => {
    const peopleStore = usePeopleStore()
    // Преобразование списка людей в формат JSON и сохранение в локальное хранилище
    localStorage.setItem('people', JSON.stringify(peopleStore.people))
}