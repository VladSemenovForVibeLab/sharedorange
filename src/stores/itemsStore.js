import { defineStore } from 'pinia'
import {usePeopleStore} from "@/stores/peopleStore";

export const useItemsStore = defineStore('items', {
    state: () => ({
        items: [], // Массив для хранения информации о товарах
    }),
    getters: {
        totalSum: (state) => {
            return state.items.reduce((sum, item) => sum + parseFloat(item.price),0);
        },
    },
    actions: {
        // Добавление нового товара в хранилище
        addItem(item) {
            item.selectedItems = []; // Добавление свойства selectedItems к каждому товару
            this.items.push(item); // Добавление товара в массив товаров
            savePeopleToLocalStorage()
        },
        // Получение данных о товаре по его идентификатору
        getItemById: (state) => (itemId) => {
            return state.items.find((item) => item.id === itemId) // Поиск товара по id
        },
        // Удаление товара по его идентификатору
        removeItem(itemId) {
            const itemIndex = this.items.findIndex((item) => item.id === itemId);
            if (itemIndex !== -1) {
                this.items.splice(itemIndex, 1); // Удаление товара из массива по индексу
                savePeopleToLocalStorage();
            }
        }
    },
})
const savePeopleToLocalStorage = () => {
    const peopleStore = usePeopleStore() // Хранилище людей
    const peopleData = JSON.stringify(peopleStore.people.value);
    localStorage.setItem('peopleData', peopleData);
};